!#/bin/sh

yosys -p "synth_ecp5 -json sys.json"  main.v rvcorem.v microc.v mmu.v mem2.v loader.v disk.v console.v spi.v pll80.v

nextpnr-ecp5 --85k --package CABGA381 --json sys.json --lpf ulx3s_v20.lpf --textcfg sys.cfg

ecppack --compress sys.cfg sys.bit

