`timescale 1ns/1ns

module sys_tb;

  reg  sysclk = 0;
  always #20 sysclk = !sysclk;

  wire [15:0] sd_data;
  wire [12:0] sd_addr;
  wire sd_csn, sd_wen, sd_rasn, sd_casn, clk_sdr, sd_cke;
  wire [1:0] sd_ba, sd_dqm;
  
  wire RX;
  reg  TX = 1;
  
  wire [3:0] sd_d;
  wire       sd_clk, sd_cmd;

  // RVSoC system
  m_main tut(
    .clk_25mhz(sysclk),
    .btn(7'h01),
    .sw(4'h0),

    .ftdi_rxd(RX),
    .ftdi_txd(TX),
    
		.sd_d(sd_d),
    .sd_clk(sd_clk),
    .sd_cmd(sd_cmd),
    
    .sdram_d(sd_data),
    .sdram_a(sd_addr),
    .sdram_ba(sd_ba),
    .sdram_dqm(sd_dqm),
    .sdram_csn(sd_csn),
    .sdram_wen(sd_wen),
    .sdram_rasn(sd_rasn),
    .sdram_casn(sd_casn),
    .sdram_clk(clk_sdr),
    .sdram_cke(sd_cke)
  );

  // simulated SDRAM, only 8MB
  SDRAM_TB sdram(
    .sdram_d(sd_data),
    .sdram_a(sd_addr),
    .sdram_ba(sd_ba),
    .sdram_csn(sd_csn),
    .sdram_wen(sd_wen),
    .sdram_rasn(sd_rasn),
    .sdram_casn(sd_casn),
    .sdram_clk(clk_sdr)
  );
  
    // Emulate SD Card
  SDCARD_TB sdcard (
    .clk(sysclk),
    .SCLK(sd_clk),
    .MOSI(sd_cmd),
    .MISO(sd_d[0]),
    .SS(sd_d[3])
  );

  
  integer i;
  initial begin
    $dumpfile("sys.fst");
    $dumpvars(0,sys_tb);
    for(i=0; i<500; i=i+1) begin

      case (i)
/*
      // loader test
      1:  begin
          hb_send(8'h20); hb_send(8'hff);
          hb_send(8'h80); hb_send(8'h00); hb_send(8'h00); hb_send(8'h00); hb_send(8'h08); hb_send(8'h90); // set len = 8
          hb_send(8'h80); hb_send(8'h00); hb_send(8'h02); hb_send(8'h00); hb_send(8'h00); hb_send(8'hb0); // set speed fast
          fb_send(8'h80); fb_send(8'h80); fb_send(8'h00); fb_send(8'h00); fb_send(8'h00); fb_send(8'ha0); // load 0x80000000
          fb_send(8'h00); fb_send(8'h80); fb_send(8'h10); fb_send(8'h3c);
          fb_send(8'h00); fb_send(8'h00); fb_send(8'h10); fb_send(8'h26);
          fb_send(8'h81); // send CRC
          #5000;
          fb_send(8'h80); fb_send(8'h00); fb_send(8'h00); fb_send(8'h00); fb_send(8'h00); fb_send(8'hb0); // set speed slow
          hb_send(8'h80); hb_send(8'h00); hb_send(8'h00); hb_send(8'h00); hb_send(8'h08); hb_send(8'h90); // set len = 8
          hb_send(8'h80); hb_send(8'h80); hb_send(8'h00); hb_send(8'h00); hb_send(8'h00); hb_send(8'ha0); // load 0x80000000
          hb_send(8'h00); hb_send(8'h80); hb_send(8'h10); hb_send(8'h3c);
          hb_send(8'h00); hb_send(8'h00); hb_send(8'h10); hb_send(8'h26);
          hb_send(8'h81); // send CRC
          end
      2:  hb_send(8'hb1); // load done
*/
      4: hb_send(8'h78);
      //15: hb_send(8'h62);
      //16: hb_send(8'h63);
      5: hb_send(8'h0d);
      endcase

      $display("ms = %d", i);
      //if (i==0)   $dumpoff();
      if (i==150) $dumpon();
      //if (i==100) $dumpoff();
      #1_000_000;
    end
    //$writememh("sdram.txt", sdram.mem);
    //$writememh("cram.txt",  tut.c.sdram.cram.mem);
    //$writememh("brom.txt",  tut.c.sdram.brom.mem);
    $finish;
  end
  
    // display bytes received on the serial line
  always @(posedge sysclk)
  begin : rcvr
    reg [7:0] byte;
    integer i;
    if (RX==0) begin
      #390
      byte = 8'h00;
      for(i=0; i<8; i=i+1)
      begin
        byte[i] = RX;
        #260;
      end
      $write("%c", byte);
    end
  end
    
  // send a byte on the serial line
  task hb_send;
    input [7:0] byte;
    integer i;
    begin
      TX = 0;
      #260;
      for(i=0; i<8; i=i+1)
        begin
        TX = byte[i];
        #260;
        end
      TX = 1;
      #520;
    end
  endtask
  
    // send a byte on the serial line fast
  task fb_send;
    input [7:0] byte;
    integer i;
    begin
      TX = 0;
      #120;
      for(i=0; i<8; i=i+1)
        begin
        TX = byte[i];
        #120;
        end
      TX = 1;
      #240;
    end
  endtask

endmodule

module SDRAM_TB (
  input  wire        sdram_clk,
  input  wire        sdram_cke,
  input  wire        sdram_csn,
  input  wire        sdram_wen,
  input  wire        sdram_rasn,
  input  wire        sdram_casn,
  input  wire  [1:0] sdram_ba,
  input  wire [12:0] sdram_a,
  inout  wire [15:0] sdram_d,
  input  wire  [1:0] sdram_dqm
);

  reg  [15:0] mem[0:16777215]; // 32MB
  
  // compose the command
  wire  [3:0] cmd;
  assign cmd[0] = sdram_wen;
  assign cmd[1] = sdram_casn;
  assign cmd[2] = sdram_rasn;
  assign cmd[3] = sdram_csn;

  // all possible commands
  localparam CMD_INHIBIT         = 4'b1111;
  localparam CMD_NOP             = 4'b0111;
  localparam CMD_ACTIVE          = 4'b0011;
  localparam CMD_READ            = 4'b0101;
  localparam CMD_WRITE           = 4'b0100;
  localparam CMD_BURST_TERMINATE = 4'b0110;
  localparam CMD_PRECHARGE       = 4'b0010;
  localparam CMD_AUTO_REFRESH    = 4'b0001;
  localparam CMD_LOAD_MODE       = 4'b0000;

  reg  [2:0] rd_sh = 0;
  reg [15:0] data;
  reg [23:0] ab;
  reg        rd = 0 , wr = 0;

  always @(posedge sdram_clk) begin
  
    // default settings; data out has 2ns hold time
    rd_sh   <= { rd_sh[1:0], rd };

    // handle command
    case (cmd)
    
    CMD_ACTIVE:
      begin
        ab[21: 9] <= sdram_a[12:0];
        ab[23:22] <= sdram_ba; // 32MB only
      end

    CMD_READ:
      begin
        ab[8:0] <= sdram_a[8:0];
        rd_sh <= 3'b001;
        rd <= 1'b1;
      end
      
    CMD_WRITE:
      begin
        ab[8:0] <= sdram_a[8:0];
        data    <= sdram_d;
        wr <= 1'b1;
      end
    
    CMD_AUTO_REFRESH:
      ; /* no need to refresh block RAM */

    CMD_BURST_TERMINATE:
      begin
        rd <= 1'b0;
        wr <= 1'b0;
      end

    CMD_PRECHARGE,      
    CMD_LOAD_MODE:
      ; /* ignore for now */

    CMD_INHIBIT,        
    CMD_NOP:
      ; /* do nothing */

    default:
      ; /* do nothing */

    endcase
    
    // write data, latch in data on the next clock;
    if (wr) begin
      mem[ab] <= data;
      data    <= sdram_d;
      ab <= ab + 1;
    end
    
    if (rd_sh[1]) begin
      data <= mem[ab];
      ab <= ab + 1;
    end;

  end
  
  // read data, latency is 2 clocks + 7ns for CAS LATENCY==3
  // hold time is 2ns
  //
  assign #7 sdram_d = (rd_sh[2]) ? data : 16'bz;

  integer i;
  initial begin
    for(i=0; i<16777216; i=i+1) mem[i] = 0;
    $readmemh("knl.hex", sdram.mem);
  end

endmodule


module SDCARD_TB (
  input  wire clk,
  input  wire SCLK,
  input  wire MOSI,
  output wire MISO,
  input  wire SS
);
  integer fh;

  initial begin
    fh = $fopen("../binary/root.bin", "a+b");
  end

  reg [47:0] in = -1;
  reg [47:0] cmd;

  always @(posedge SCLK) begin
    if (sync) begin
      in  <= 48'hffffffffffff;
    end
    else
      in <= { in[46:0], MOSI };
  end
  
  wire sync = !in[47];
  
  reg [7:0] out;

  always @(posedge SCLK) begin
    if (sync) begin
      cmd <= in;
      out <= 8'h00;
      $display("SD CMD = %h", in);
    end
    else
      out <= { out[6:0], 1'b1 };
  end
  
  wire rmod = (in[45:40]==5'h11) ? 1 : 0;
  wire read = rmod | !holding;
  
  reg [12:0] ctr = 513;
  reg  [7:0] byte;
  reg        holding = 1'b1;

  integer n, sec;

  always @(negedge SCLK) begin
    if (rmod & holding) begin
      ctr     <= 0;
      byte    <= 0;
      holding <= 1'b0;
      sec = (in[39:8]) * 512;
      n <= $fseek(fh, sec, 0);
    end
    else if (!holding) begin
      ctr <= ctr + 1;
      byte <= { byte[6:0], 1'b1 };
      
      if (ctr[2:0]==3'b111) begin
        case (ctr[12:3])
              0:  byte <= 8'hfe;
        default:  begin
                  n <= $fread(byte, fh);
                  //$fwrite(fh, "%c", data);
                  //$write("%x", byte);
                  byte <= byte;
                  end
            513:  holding <= 1'b1;
        endcase
      end
    end
  end

  assign MISO = (!read) ? out[7] : byte[7];

endmodule
