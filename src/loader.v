/**************************************************************************************************/
/**** RVSoc (Mini Kuroda/RISC-V)                       since 2018-08-07   ArchLab. TokyoTech   ****/
/**** ALU Module v0.01                                                                         ****/
/**************************************************************************************************/
`default_nettype none
/**************************************************************************************************/
`include "define.vh"

/**************************************************************************************************/
/* Clock Interval Definition                                                                      */
/**************************************************************************************************/
// b = baud rate (in Mbps)
// f = frequency of the clk for the processor core (in MHz)
// SERIAL_WCNT = f/b
// e.g. b = 1, f = 50 -> SERIAL_WCNT = 50/1 = 50
//`ifndef SERIAL_WCNT
`ifndef __ICARUS__
`define SERIAL_WCNT 347 // 115K2
`define FAST_WCNT   40  //174 // 13  // 3M
`define SLOW_WCNT   347  //347 // 115K2
`else
`define SERIAL_WCNT 13  
`define FAST_WCNT    6  
`define SLOW_WCNT   13  
`endif
//`endif

/**************************************************************************************************/
module UartTx(CLK, RST_X, data, we, TXD, ready, fast);
    input  wire        CLK, RST_X, we;
    input  wire [7:0]  data;
    output reg         TXD, ready;
    input  wire        fast;

    reg [8:0]   cmd;
    reg [31:0]  waitnum;
    reg [3:0]   cnt;

    always @(posedge CLK) begin
        if(!RST_X) begin
            TXD       <= 1'b1;
            ready     <= 1'b1;
            cmd       <= 9'h1ff;
            waitnum   <= 0;
            cnt       <= 0;
        end else if( ready ) begin
            TXD       <= 1'b1;
            waitnum   <= 0;
            if( we )begin
                ready <= 1'b0;
                cmd   <= {data, 1'b0};
                cnt   <= 10;
            end
        end else if( waitnum >= (fast ? `FAST_WCNT : `SLOW_WCNT) ) begin
            TXD       <= cmd[0];
            ready     <= (cnt == 1);
            cmd       <= {1'b1, cmd[8:1]};
            waitnum   <= 1;
            cnt       <= cnt - 1;
        end else begin
            waitnum   <= waitnum + 1;
        end
    end
endmodule

/**************************************************************************************************/
/* RS232C serial controller (deserializer):                                                       */
/**************************************************************************************************/
`define SS_SER_WAIT  'd0         // RS232C deserializer, State WAIT
`define SS_SER_RCV0  'd1         // RS232C deserializer, State Receive 0th bit
                                 // States Receive 1st bit to 7th bit are not used
`define SS_SER_DONE  'd9         // RS232C deserializer, State DONE
/**************************************************************************************************/
module UartRx(CLK, RST_X, RXD, data, en, fast);
    input  wire   CLK, RST_X, RXD; // clock, reset, RS232C input
    output [7:0]  data;            // 8bit output data
    output reg    en;              // 8bit output data enable
    input  wire   fast;

    reg    [7:0]   data;
    reg    [3:0]   stage;
    reg    [12:0]  cnt;             // counter to latch D0, D1, ..., D7
    reg    [11:0]  cnt_start;       // counter to detect the Start Bit
    reg    [12:0]  waitcnt;

    always @(posedge CLK) begin
        if (!RST_X) cnt_start <= 0;
        else        cnt_start <= (RXD) ? 0 : cnt_start + 1;
    end
    
    always @(posedge CLK) begin
        if(!RST_X) begin
            en     <= 0;
            stage  <= `SS_SER_WAIT;
            cnt    <= 1;
            data   <= 0;
        end else if (stage == `SS_SER_WAIT) begin // detect the Start Bit
            waitcnt <= fast ? `FAST_WCNT : `SLOW_WCNT;
            en <= 0;
            stage <= (cnt_start == (waitcnt >> 1)) ? `SS_SER_RCV0 : stage;
        end else begin
            if (cnt != waitcnt) begin
                cnt <= cnt + 1;
                en <= 0;
            end else begin               // receive 1bit data
                stage  <= (stage == `SS_SER_DONE) ? `SS_SER_WAIT : stage + 1;
                en     <= (stage == 8)  ? 1 : 0;
                data   <= {RXD, data[7:1]};
                cnt <= 1;
            end
        end
    end
endmodule

/**************************************************************************************************/

module FLOADER (CLK, RST_X, RXD, addr, data, we, done, key_we, key_data, TXD, do_load, mbsy);
    input  wire         CLK, RST_X, RXD;
    output reg  [31:0]  addr;
    output reg  [31:0]  data;
    output reg          we;
    output reg          done;
    output wire         key_we;
    output wire [7:0]   key_data;
    output wire         TXD;
    input  wire         do_load;
    input  wire         mbsy;

    reg [31:0] len;  // length register
    reg [31:0] crc;  // crc register
    reg        fast; // 115K2 or 3M baud
    reg        tx;   // start transmission of a crc byte
    reg  [4:0] s;    // protocol state

    wire       tick, tock;
    wire [7:0] c;
    
    UartRx seri (CLK, RST_X, RXD, c, tick, fast);
    UartTx sero (CLK, RST_X, crc[31:24], tx, TXD, tock, fast);

    assign key_we   = tick && done;
    assign key_data = c;

    localparam RST = 5'h00, CMD = 5'h02, LD  = 5'h03, BLK = 5'h17,
               CHK = 5'h0b, STP = 5'h0f, SYN = 5'h07;

    always @(posedge CLK) begin
      if(!RST_X) begin
          fast <= 1; tx <= 0; data <= 0; done <= 0; we <= 0;
          s <= do_load ? RST : STP; addr <= 32'h80000000;
      end else begin
        if(!mbsy) we <= 0;
        tx <= 0; done <= 0;
        if(tick & !mbsy) begin
          case (s)
          RST:    if(c == 8'hfe) s <= RST+1;    // sync on 0xFF + 0x80
          RST+1:  s <= (c == 8'h80) ? LD : RST;

          CMD:    case (c)
                  8'h80: s <= LD;
                  8'h81: begin tx <= 1; s <= CHK; end
                  8'h90: len <= data;
                  8'ha0: begin
                           addr <= data & 32'hfffffffc;
                           crc  <= 0;
                           s    <= BLK;
                         end
                  8'hb0: begin
                           fast <= (data[19]!=0) ? 1 : 0;
                         end
                  default: s <= STP;
                  endcase

          LD:     begin data <= {data[23:0], c}; s <= LD+1;   end
          LD+1:   begin data <= {data[23:0], c}; s <= LD+2;   end
          LD+2:   begin data <= {data[23:0], c}; s <= LD+3;   end
          LD+3:   begin data <= {data[23:0], c}; s <= CMD;    end
          
          BLK:    begin data <= {c, data[31:8]}; s <= BLK+1;  end
          BLK+1:  begin data <= {c, data[31:8]}; s <= BLK+2;  end
          BLK+2:  begin data <= {c, data[31:8]}; s <= BLK+3;  end
          BLK+3:  begin data <= {c, data[31:8]};
                        we   <= 1;
                        s    <= BLK+4;
                  end
          BLK+4:  begin data <= {c, data[31:8]};
                        addr <= addr + 4;
                        s    <= BLK+1;
                  end
                  
          endcase
        end
        
        if(tock & !tx) begin
          case (s)
          CHK:    begin crc <= crc << 8; s <= CHK+1; tx <= 1; end
          CHK+1:  begin crc <= crc << 8; s <= CHK+2; tx <= 1; end
          CHK+2:  begin crc <= crc << 8; s <= CHK+3; tx <= 1; end
          CHK+3:  begin s <= CMD;                             end
          endcase
        end
        
        if(s==STP) begin done <= 1; fast <= 0; end
        
        if(s[4] & tick) begin
          crc <= {crc[30:0], crc[31]} + c;
          len <= len - 1;
          if(len==1) s <= CMD;
        end

      end
    end

endmodule

