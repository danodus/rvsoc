/**************************************************************************************************/
/**** RVSoC (Mini Kuroda/RISC-V)                       since 2018-08-07   ArchLab. TokyoTech   ****/
/**** main module for Implemetaion v0.13                                                       ****/
/**************************************************************************************************/
`default_nettype none
/**************************************************************************************************/
`include "define.vh"

(* top *)
module m_main(
    // Master clock
    input  wire        clk_25mhz,
    output wire        wifi_en,         // enable ESP32
    output wire        wifi_gpio0,      // quiet ESP32

    
    // UART signals
    input  wire        ftdi_txd,        // host -> SoC
    output wire        ftdi_rxd,        // SoC -> host
    
    // LEDs and buttons
    input  wire  [3:0] sw,              // user switches
    input  wire  [6:0] btn,             // user buttons
    output wire  [7:0] led,             // user leds

    // SDRAM signals
    output wire        sdram_clk,
    output wire        sdram_cke,
    output wire        sdram_csn, 
    output wire        sdram_rasn,
    output wire        sdram_casn,
    output wire        sdram_wen,
    output wire [12:0] sdram_a,
    output wire  [1:0] sdram_ba,
    output wire  [1:0] sdram_dqm,
    inout  wire [15:0] sdram_d,
    
    inout  wire        sd_clk,          // SD_CK
    inout  wire        sd_cmd,          // SD_DI
    inout  wire  [3:0] sd_d             // [0] = SD_DO, [3] = SD_nCS

  );

    wire RST_X_IN = 1;
    wire w_btnu = 0;
    wire w_btnd = 0;
    wire w_btnl = 0;
    wire w_btnr = 0;
    wire w_btnc = 0;
    wire [15:0] w_sw = 0;
    
    assign wifi_en    = 0;
    //assign wifi_gpio0 = 0;

    wire [15:0] w_led_t; // temporary w_led

    reg [31:0] r_cnt=0;
    reg        r_time_led=0;
    
    always@(posedge cpu_clk) r_cnt <= (r_cnt>=(64*1000000/2-1)) ? 0 : r_cnt+1;
    always@(posedge cpu_clk) r_time_led <= (r_cnt==0) ? !r_time_led : r_time_led;

    assign led[0] = r_time_led;
    assign led[1] = r_time_led & !w_init_done;
    //assign led[2] = w_data_we;
    assign led[2] = w_busy;
    assign led[6:3] = { sd_clk, sd_cmd, sd_d[0], sd_d[3] };
    assign led[7] = RST_X;
    
    /*******************************************************************************/
    reg         r_stop = 0;
    reg  [63:0] r_core_cnt = 0;

    // Connection Core <--> mem_ctrl
    wire [31:0] w_insn_data, w_insn_addr;
    wire [31:0] w_data_data, w_data_wdata, w_data_addr;
    wire        w_data_we;
    wire  [2:0] w_data_ctrl;

    wire [31:0] w_priv, w_satp, w_mstatus;
    wire [63:0] w_mtime, w_mtimecmp, w_wmtimecmp;
    wire        w_clint_we;
    wire [31:0] w_mip, w_wmip;
    wire        w_plic_we;
    wire        w_busy;
    wire [31:0] w_pagefault;
    wire  [1:0] w_tlb_req;
    wire        w_tlb_flush;
    wire        w_init_done;
    wire        w_init_stage;

    // Clock
    wire w_locked, ref_clk;
    PLL pll (
      .clkin   (clk_25mhz),
      .pll100  (ref_clk),
      .locked  (w_locked)
    );

    reg [2:0] clk = 2'b0;
    reg cpu_clk, ram_clk;
    always @(posedge ref_clk) begin
      clk <= clk + 1;
      cpu_clk = clk[1];
      ram_clk = clk[0];
    end

    // Reset
    reg [14:0] rctr = 0;
    always @(posedge clk_25mhz) rctr <= (!w_locked || !btn[0]) ? 0 : rctr + !rctr[14];

    wire RST_X      = rctr[14];
    wire CORE_RST_X = RST_X & w_init_done;

    wire [31:0] w_core_odata;
    wire [31:0] w_core_pc, w_core_ir;
    wire w_init_start;
    reg  r_load = 0;
    always@(posedge cpu_clk) begin
        if(r_load==0 && w_init_start && !w_init_done) r_load <= 1;
        else if(w_init_done) r_load <= 0;
    end
  
    // Uart
    wire  [7:0] w_uart_data;
    wire        w_uart_we;
    wire [31:0] w_checksum;

    wire w_finish;
    wire w_halt;

    // stop and count
    always@(posedge cpu_clk) begin
        if(w_btnc | w_halt | w_finish) r_stop <= 1;
        if(w_init_done && !r_stop && w_led_t[9:8] == 0) r_core_cnt <= r_core_cnt + 1;
    end

    MMU c(
        .CLK            (cpu_clk),
        .RST_X          (RST_X),

        .w_insn_addr    (w_insn_addr),
        .w_data_addr    (w_data_addr),
        .w_data_wdata   (w_data_wdata),
        .w_data_we      (w_data_we),
        .w_data_ctrl    (w_data_ctrl),
        .w_insn_data    (w_insn_data),
        .w_data_data    (w_data_data),
        .r_finish       (w_finish),
        .w_priv         (w_priv),
        .w_satp         (w_satp),
        .w_mstatus      (w_mstatus),
        .w_mtime        (w_mtime),
        .w_mtimecmp     (w_mtimecmp),
        .w_wmtimecmp    (w_wmtimecmp),
        .w_clint_we     (w_clint_we),
        .w_mip          (w_mip),
        .w_wmip         (w_wmip),
        .w_plic_we      (w_plic_we),
        .w_proc_busy    (w_busy),
        .w_pagefault    (w_pagefault),
        .w_tlb_req      (w_tlb_req),
        .w_tlb_flush    (w_tlb_flush),
        .w_txd          (ftdi_rxd),
        .w_rxd          (ftdi_txd),
        .w_init_done    (w_init_done),

        .ram_clk        (ram_clk),
        .sd_data        (sdram_d),    // SDRAM chip side
        .sd_addr        (sdram_a),
        .sd_dqm         (sdram_dqm),
        .sd_ba          (sdram_ba),
        .sd_cs          (sdram_csn),
        .sd_we          (sdram_wen),
        .sd_ras         (sdram_rasn),
        .sd_cas         (sdram_casn),
        .sd_cke         (sdram_cke),
        .sd_clk         (sdram_clk),

        .MISO           (sd_d[0]),    // SD Card in SPI mode
        .MOSI           (sd_cmd),
        .SCLK           (sd_clk),
        .SnCS           (sd_d[3]),

        .w_uart_data    (w_uart_data),
        .w_uart_we      (w_uart_we),
        .w_led          (w_led_t),
        .w_init_stage   (w_init_stage),
        .w_checksum     (w_checksum),
        .w_init_start   (w_init_start),
        .w_load         (sw[0]|btn[1])  // set sw0 to 1 for fujprog program load
    );

    m_RVCoreM p(
        .CLK            (cpu_clk),
        .RST_X          (CORE_RST_X),
        .w_stall        (r_stop),
        .r_halt         (w_halt),
        .w_insn_addr    (w_insn_addr),
        .w_data_addr    (w_data_addr),
        .w_insn_data    (w_insn_data),
        .w_data_data    (w_data_data),
        .w_data_wdata   (w_data_wdata),
        .w_data_we      (w_data_we),
        .w_data_ctrl    (w_data_ctrl),
        .w_priv         (w_priv),
        .w_satp         (w_satp),
        .w_mstatus      (w_mstatus),
        .w_mtime        (w_mtime),
        .w_mtimecmp     (w_mtimecmp),
        .w_wmtimecmp    (w_wmtimecmp),
        .w_clint_we     (w_clint_we),
        .w_mip          (w_mip),
        .w_wmip         (w_wmip),
        .w_plic_we      (w_plic_we),
        .w_busy         (w_busy),
        .w_pagefault    (w_pagefault),
        .w_tlb_req      (w_tlb_req),
        .w_tlb_flush    (w_tlb_flush),
        .w_core_pc      (w_core_pc),
        .w_core_ir      (w_core_ir),
        .w_core_odata   (w_core_odata),
        .w_init_stage   (w_init_stage)
    );

endmodule


