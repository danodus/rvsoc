/*
 * Basic SPI interface for SD Cards:
 * 1-bit 10Mhz data transfers @ 40MHz clock in
 *
 * Addressed via two registers:
 * addr = 0x...0: status register, bit 31 is speed, bit 0 is start
 * addr = 0x...4: data r/w register
 * Only handles 32 bit naturally aligned accesses
 *
 * This source code is public domain
 *
 */

module SPI(
    input  wire        clk,
    input  wire        rst,

    input  wire        s_we,
    input  wire [31:0] s_addr,
    input  wire [31:0] s_din,
    output wire [31:0] s_dout,
    output wire        s_bsy,

    input  wire        MISO,
    output wire        MOSI,
    output wire        SCLK,
    output wire        SnCS
  );

  // status register, has just two bits implemented:
  reg fast, sel;
  always @(posedge clk) begin
    fast <= (s_we && !s_addr[2]) ? s_din[1] : fast;
    sel  <= (s_we && !s_addr[2]) ? s_din[0] : sel;
    if(rst) begin fast <= 0; sel <= 0; end
  end
  wire start = s_we && s_addr[2];

  // clock divider into 400kHz / 10MHz signal
  reg  [6:0] clkcnt;
  always @ (posedge clk) clkcnt <= (rst | rdy | tick) ? 0 : clkcnt + 1;
  wire       tick = fast ? (clkcnt == 3) : (clkcnt == 99);

  // bit counter
  reg  [4:0] bitcnt;
  always @ (posedge clk) bitcnt <= (rst | start)    ? 0 :
                                   (tick & ~endbit) ? bitcnt + 1
                                                    : bitcnt;
  wire       endbit = fast ? (bitcnt == 31) : (bitcnt == 7);

  // shifter: 32 bits come LSB first, so loop bits through shifter word
  // by reverse byte when in fast (= 32 bit) mode
  reg [31:0] shift;
  reg        rdy;
  always @(posedge clk) begin

    rdy <= (rst | (tick & endbit)) ? 1 :
           start                   ? 0
                                   : rdy;

    shift <= rst   ? -1 :
             start ? s_din :
             tick  ? {shift[30:24],      MISO,
                      shift[22:16], shift[31],
                      shift[14: 8], shift[23],
                      shift[ 6: 0], (fast ? shift[15] : MISO)} 
                   : shift;
  end

  // assign output signals
  assign MOSI = (rst | rdy) ? 1 : shift[7];
  assign SCLK = (rst | rdy) ? 0 : fast ? tick : clkcnt[6];
  assign SnCS = ~sel;

  assign s_dout = s_addr[2] ? (fast ? shift : {24'b0, shift[7:0]})
                            : ({30'b0, fast, sel}) ;
  assign s_bsy = ~rdy;

endmodule
