
/*
 * RVSoC Memory
 * - bus adapter
 * - boot rom/ram (4KB + 4KB) at address zero
 * - 16KB four way, set associative cache w/ 256 byte cache lines
 * - SDRAM controller
 *
 * This source code is public domain
 *
 */

module MEMORY (

  // CPU interface
  input  wire        cpu_clk,     // CPU clk
  input  wire        ram_clk,     // SDR clk
  input  wire        rst,         // reset

  input  wire [31:0] m_addr,      // address
  input  wire [31:0] m_din,       // data to cache
  output wire [31:0] m_dout,      // data to CPU
  input  wire  [2:0] m_ctrl,      // partial word mask
  input  wire        m_rd,        // request mem RD transaction
  input  wire        m_wr,        // request mem WR transaction
  output wire        m_bsy,       // report mem ready

  inout  wire [15:0] sd_data,     // SDRAM signals
  output wire [12:0] sd_addr,
  output wire  [1:0] sd_dqm, 
  output wire  [1:0] sd_ba,  
  output wire        sd_cs,  
  output wire        sd_we,  
  output wire        sd_ras, 
  output wire        sd_cas, 
  output wire        sd_cke, 
  output wire        sd_clk
);

  // ---------------------------------------------------------------------
  // ----------------- bus interface & boot rom/ram  ---------------------
  // ---------------------------------------------------------------------
  
  // Latch rd selection lines
  //
  reg  [2:0] ctrl;
  reg [31:0] addr;
  reg [31:0] din;
  reg        rd, wr, boot;
  always @(posedge cpu_clk) begin
    if(rst) begin rd <= 1'b0; wr <= 1'b0; end;
    if(m_rd|m_wr) begin
      addr <= m_addr; ctrl <= m_ctrl; din <= m_din;
      rd <= m_rd; wr <= m_wr;
    end
    if(!m_bsy) begin rd <= m_rd; wr <= m_wr; end
    boot <= (m_addr[31:14]==0);
  end
    
  reg mstat, xreq;
  always @(posedge cpu_clk) begin
    if(rst)                     mstat <= 0;
    if((m_rd|m_wr) & mstat==0)  mstat <= 1;
    if(mstat==1 & (valid|boot)) mstat <= 0;
    xreq <= m_req;
  end;
  wire req = xreq & !boot;
  
  wire m_req   = (m_rd|m_wr) & !rst;
  wire valid   = m_req & !m_bsy & hit;
  assign m_bsy = !mrdy | (m_req & ((!hit & !boot) | mstat==0 | cstat!=0));
  
  // Handle partial words for output data: align lowest byte & extend
  // note: does not handle mis-alignment
  //
  wire [31:0] x_dout = boot ? b_dout : c_dout;
  wire [31:0] dout = x_dout >> { addr[1:0], 3'b0 };

  assign m_dout = (ctrl[1:0]==0) ? ((ctrl[2]) ? {24'h0, dout[7:0]}
                                              : {{24{dout[7]}}, dout[7:0]}) :
                  (ctrl[1:0]==1) ? ((ctrl[2]) ? {16'h0, dout[15:0]}
                                              : {{16{dout[15]}}, dout[15:0]})
                                 : dout;

  //Handle partial words for input data: replicate and form wmask
  //
  wire [31:0] xin = (ctrl[1:0] == 0) ? {4{din[ 7:0]}} :   //  8 bit write
                    (ctrl[1:0] == 1) ? {2{din[15:0]}} :   // 16 bit write
                                          din;            // 32 bit write

  wire  [3:0] wmask = (ctrl[1:0]==2'b10) ?  4'b1111 :
                      (ctrl[1:0]==2'b01) ? (4'b0011 << {addr[1], 1'b0}) :
                                           (4'b0001 <<  addr[1:0]) ;

  // Boot ROM / RAM
  //
  wire [31:0] b_dout;
  wire        b_write = boot & wr; // & addr[12];
  wire  [3:0] b_wmask = {4{b_write}} & wmask;
  
  BROM brom( .b_clk(cpu_clk), .b_addr(addr[13:2]), .b_din(xin),
             .b_dout(b_dout), .b_re(rd & boot), .b_wmask(b_wmask) );

  // ---------------------------------------------------------------------
  // ------------------------ cache tag management -----------------------
  // ---------------------------------------------------------------------

  localparam bWAY = 2, WAYS = (1<<bWAY);  // 2 bits,  4 way
  localparam bSET = 4, SETS = (1<<bSET);  // 4 bits, 16 sets

  // { [31:12] tag ; [11:8] set ; [7:0] (byte) cache line }
  //
  wire  [bSET-1:0] set = addr[bSET+7:8];
  wire [23-bSET:0] tag = addr[31:bSET+8];

  // For each way in the cache define a tag table & comparator
  //
  reg  [23-bSET:0] tag0[0:SETS-1], tag1[0:SETS-1], tag2[0:SETS-1], tag3[0:SETS-1];
  
  wire fnd0 = (tag0[set] == tag), fnd1 = (tag1[set] == tag);
  wire fnd2 = (tag2[set] == tag), fnd3 = (tag3[set] == tag);
  wire hit  = fnd0 | fnd1 | fnd2 | fnd3;

  // Init tags to be non-conflicting
  //
  integer j;
  initial begin
      for(j=0; j<SETS; j=j+1) begin
        tag0[j]   = 20'h90000;      tag1[j] = 20'h90001;      tag2[j] = 20'h90002;      tag3[j] = 20'h90003;
        dirty0[j] = 1'b0; dirty1[j] = 1'b0; dirty2[j] = 1'b0; dirty3[j] = 1'b0;
      end
  end

  // Replacement policy: "Random"
  //
  reg [bWAY-1:0] repl = 0;
  always @(posedge cpu_clk) repl <= repl + 1;
  
  // Update tag bits
  //
  reg   [bWAY-1:0]  nidx;  // idx to replace
  reg  [23-bSET:0]  wtag;  // tag to save
  reg  [23-bSET:0]  rtag;  // tag to read
  reg               dirty; // save necessary
  
  reg   [SETS-1:0]  dirty0, dirty1, dirty2, dirty3;   // dirty bits per set
  
  // These three are used to sync the CPU and the RAM clock domains
  //
  reg start, stop, ackn;

  reg [1:0] cstat = 0;
  always @(posedge cpu_clk) begin
    case(cstat)

    0:  begin
          rtag  <= tag;
          nidx  <= repl;
          start <= 1'b0; ackn <= 1'b1;
          if(req & !hit) cstat <= 1;
        end
        
    1:  begin
          if (nidx==0) begin wtag <= tag0[set]; tag0[set] <= rtag; dirty <= dirty0[set]; end
          if (nidx==1) begin wtag <= tag1[set]; tag1[set] <= rtag; dirty <= dirty1[set]; end
          if (nidx==2) begin wtag <= tag2[set]; tag2[set] <= rtag; dirty <= dirty2[set]; end
          if (nidx==3) begin wtag <= tag3[set]; tag3[set] <= rtag; dirty <= dirty3[set]; end
          start <= 1'b1; ackn <= 1'b0;
          cstat <= 2;
        end

    2:  begin
          start <= 1'b0;
          if(stop) cstat <= 0;
        end

    endcase
  end

  // Update dirty bits
  //
  always @(posedge cpu_clk) begin
    if(valid & !boot) begin
      if (fnd0) dirty0[set] <= dirty0[set] | wr;
      if (fnd1) dirty1[set] <= dirty1[set] | wr;
      if (fnd2) dirty2[set] <= dirty2[set] | wr;
      if (fnd3) dirty3[set] <= dirty3[set] | wr;
    end
    if(cstat==2) begin
      if (fnd0) dirty0[set] <= 1'b0;
      if (fnd1) dirty1[set] <= 1'b0;
      if (fnd2) dirty2[set] <= 1'b0;
      if (fnd3) dirty3[set] <= 1'b0;
    end
  end

  // ---------------------------------------------------------------------
  // --------------------- cacheline update controller -------------------
  // ---------------------------------------------------------------------

  reg mrdy;
  always @(posedge cpu_clk) begin
    if(rst)         mrdy <= 1'b1;
    if(req & !hit)  mrdy <= 1'b0;
    if(!mrdy)       mrdy <= 1'b1; // FIXME
  end

  localparam STBY = 3'd0, RD = 3'd1, WT1 = 3'd2, WR = 3'd3,
             SW   = 3'd4, WT = 3'd5, WT2 = 3'd6;

  reg   [2:0] rstat;
  reg  [23:0] ram_addr;    // high part of ram address (256 byte line)
  reg         ram_rd, ram_wr;
  
  always @(posedge ram_clk) begin
    if (rst) begin rstat <= STBY; end
    else begin
    case (rstat)

    STBY: begin
            stop <= 1'b0;
            if (start) rstat <= WT;
          end
    
    WT:   begin
            ram_addr <= dirty ? { wtag, set } : { rtag, set };
            rstat    <= dirty ?      WR       :       RD;
          end
            
    // Write cache line
    WR:   if (done) begin
            ram_addr <= { rtag, set };
            rstat <= SW;
          end
    SW:   rstat <= RD;

    // Read cache line
    RD:   if (done) begin
            rstat <= WT1;
          end
    WT1:  begin
            stop <= 1'b1;
            rstat <= WT2;
          end

    WT2:  if (ackn) rstat <= STBY;

    endcase
    end
    ram_rd <= (rstat==RD);
    ram_wr <= (rstat==WR);
  end

  // Mirror SDRAM address: count out 256 bytes (128 sdram words)
  //
  reg [7:0] ram_ctr;  // mirror address
  wire      ram_get;  // signal from SDRAM to store data
  wire      ram_put;  // signal from SDRAM to send data
  wire      zctr = (rstat == WT1) | (rstat == SW);

  always @(posedge ram_clk) begin
    if (ram_get | ram_put) ram_ctr <= ram_ctr + 1;
    else if (zctr|rst)     ram_ctr <= 8'b0;
  end
  wire done = ram_ctr[7];  // full cache line processed
  
  // Cache RAM
  //
  wire     [31:0] c_dout;
  wire            c_write = !boot & wr & valid;
  wire      [3:0] c_wmask = {4{c_write}} & wmask;
  wire [bWAY-1:0] c_way = { fnd3|fnd2, fnd3|fnd1 };

  wire     [15:0] ram_dout;
  wire     [31:0] r_din;
  reg      [15:0] ram_din;
  wire      [3:0] r_wmask = {4{ram_get}} & (ram_ctr[0] ? 4'b1100 : 4'b0011);
  
  always @(posedge ram_clk) ram_din <= (!ram_ctr[0]) ? r_din[31:16] : r_din[15:0];

  CRAM cram( .c_clk(cpu_clk),
             .c_addr({ c_way, set,   addr[7:2] }),
             .c_din(xin),
             .c_dout(c_dout),
             .c_re(rd & !boot),
             .c_wmask(c_wmask),
             
             .r_clk(ram_clk),
             .r_addr({ nidx, set, ram_ctr[6:1] }),
             .r_din({ ram_dout, ram_dout }),
             .r_dout(r_din),
             .r_re(1'b1),
             .r_wmask(r_wmask)
           );
           
  SDRAM sdram( .clk_in(ram_clk),
               .din(ram_din),
               .dout(ram_dout),
               .ad(ram_addr),
               .get(ram_get),
               .put(ram_put),
               .rd(ram_rd),
               .wr(ram_wr),
               .rst(rst),
               //.calib(m_calib),
              
               .sd_data(sd_data),
               .sd_addr(sd_addr),
               .sd_dqm(sd_dqm), 
               .sd_ba(sd_ba),  
               .sd_cs(sd_cs),  
               .sd_we(sd_we),  
               .sd_ras(sd_ras), 
               .sd_cas(sd_cas), 
               .sd_cke(sd_cke), 
               .sd_clk(sd_clk) 
             );

endmodule

/*
 * Boot ROM/RAM: 8KB x 32b, single-ported 
 */

module BROM (
  input  wire        b_clk,         // SDRAM clock
  input  wire [11:0] b_addr,        // address
  input  wire [31:0] b_din,         // data to mem
  output reg  [31:0] b_dout,        // data from mem
  input  wire        b_re,          // read enable
  input  wire  [3:0] b_wmask        // byte write mask
);

  reg [31:0] mem[0:4095];

  always @(posedge b_clk) begin
    if (b_re) b_dout <= mem[b_addr];
  end
  
  always @(posedge b_clk) begin
    if (b_wmask[3]) mem[b_addr][31:24] <= b_din[31:24];
    if (b_wmask[2]) mem[b_addr][23:16] <= b_din[23:16];
    if (b_wmask[1]) mem[b_addr][15: 8] <= b_din[15: 8];
    if (b_wmask[0]) mem[b_addr][ 7: 0] <= b_din[ 7: 0];
  end

  integer i;
  initial begin
    $readmemh("../monitor/mem.bin", mem);
    //for(i=1024; i<4096; i=i+1)
    //  mem[i] = 0; 
  end

endmodule

/*
 * Dual ported cache ram
 */

module CRAM (
  input  wire        c_clk,        // cpu side
  input  wire [11:0] c_addr,
  input  wire [31:0] c_din, 
  output reg  [31:0] c_dout,
  input  wire        c_re,  
  input  wire  [3:0] c_wmask,
  
  input  wire        r_clk,        // ram side
  input  wire [11:0] r_addr,
  input  wire [31:0] r_din, 
  output reg  [31:0] r_dout,
  input  wire        r_re,  
  input  wire  [3:0] r_wmask
);

  reg [31:0] mem[0:4095];

  always @(posedge c_clk) begin
    if (c_re) c_dout <= mem[c_addr];
  end
  
  always @(posedge c_clk) begin
    if (c_wmask[3]) mem[c_addr][31:24] <= c_din[31:24];
    if (c_wmask[2]) mem[c_addr][23:16] <= c_din[23:16];
    if (c_wmask[1]) mem[c_addr][15: 8] <= c_din[15: 8];
    if (c_wmask[0]) mem[c_addr][ 7: 0] <= c_din[ 7: 0];
  end

  always @(posedge r_clk) begin
    if (r_re) r_dout <= mem[r_addr];
  end
  
  always @(posedge r_clk) begin
    if (r_wmask[3]) mem[r_addr][31:24] <= r_din[31:24];
    if (r_wmask[2]) mem[r_addr][23:16] <= r_din[23:16];
    if (r_wmask[1]) mem[r_addr][15: 8] <= r_din[15: 8];
    if (r_wmask[0]) mem[r_addr][ 7: 0] <= r_din[ 7: 0];
  end

  integer i;
  initial begin
    for(i=0; i<4096; i=i+1)
      mem[i] = (i<<1 | (((i<<1)+1) << 16));
  end

endmodule

/*
 * Matching SDRAM controller, do 256 byte bursts
 * Designed to run at ~100 MHz (i.e. will not work @>110Mhz with grade 7 sdram)
 */

module SDRAM (
  input             clk_in,     // controller clock
  
  // interface to the cache
  input      [15:0] din,        // data input from cpu
  output reg [15:0] dout,       // data output to cpu
  input      [23:0] ad,         // 23 bit upper address
  output reg        get,        // load word from SDR on sdr_clk2
  output reg        put,        // send word to SDR on sdr_clk2
  input  wire       rd,         // SDR start read transaction
  input  wire       wr,         // SDR start write transaction
  input  wire       rst,        // cpu reset
  output wire       calib,      // sdram initialising

  // interface to the chip
  inout      [15:0] sd_data,    // 16 bit databus
  output reg [12:0] sd_addr,    // 12 bit multiplexed address bus
  output reg [1:0]  sd_dqm,     // two byte masks
  output reg [1:0]  sd_ba,      // two banks
  output            sd_cs,      // chip select
  output            sd_we,      // write enable
  output            sd_ras,     // row address select
  output            sd_cas,     // column address select
  output            sd_cke,     // clock enable
  output            sd_clk      // chip clock (inverted from input clk)
);

  localparam BURST_LENGTH   = 3'b111; // 000=1, 001=2, 010=4, 011=8, 111=full page
  localparam ACCESS_TYPE    = 1'b0;   // 0=sequential, 1=interleaved
  localparam CAS_LATENCY    = 3'd3;   // 3 needed @ >100Mhz
  localparam OP_MODE        = 2'b00;  // only 00 (standard operation) allowed
  localparam WRITE_BURST    = 1'b0;   // 0=write burst enabled, 1=only single access write

  localparam MODE = {3'b000, WRITE_BURST, OP_MODE, CAS_LATENCY, ACCESS_TYPE, BURST_LENGTH};
  
  // Extend the address bus (31 bits, for 16b words)
  wire [30:0] addr;
  assign addr = { ad, 7'h0 };


  // ---------------------------------------------------------------------
  // --------------------------- startup/reset ---------------------------
  // ---------------------------------------------------------------------

  // make sure reset lasts long enough (recommended 100us)
  reg [12:0] reset;
  always @(posedge clk_in) begin
    reset <= (|reset) ? reset - 13'd1 : 0;
    if(rst)	reset <= 13'd100;
  end
  
  assign calib = |reset;

  // ---------------------------------------------------------------------
  // ------------------ generate ram control signals ---------------------
  // ---------------------------------------------------------------------

  // all possible commands
  localparam CMD_INHIBIT         = 4'b1111;
  localparam CMD_NOP             = 4'b0111;
  localparam CMD_ACTIVE          = 4'b0011;
  localparam CMD_READ            = 4'b0101;
  localparam CMD_WRITE           = 4'b0100;
  localparam CMD_BURST_TERMINATE = 4'b0110;
  localparam CMD_PRECHARGE       = 4'b0010;
  localparam CMD_AUTO_REFRESH    = 4'b0001;
  localparam CMD_LOAD_MODE       = 4'b0000;

  assign sd_clk = !clk_in; // chip clock shifted 180 deg.
  assign sd_cke = ~rst;

  // drive control signals according to current command
  reg  [3:0] sd_cmd; 
  assign sd_cs  = sd_cmd[3];
  assign sd_ras = sd_cmd[2];
  assign sd_cas = sd_cmd[1];
  assign sd_we  = sd_cmd[0];

  // sdram tri-state databus interaction
  reg  sd_rden = 0, sd_wren = 0;

`ifdef __ICARUS__
  reg   [15:0] i;
  assign sd_data  = sd_wren ? i : 16'hzzzz;
  always @(posedge sd_clk) i <= din;
  always @(posedge sd_clk) if(sd_rden) dout <= sd_data;
`else
  wire  [15:0] o;
  reg   [15:0] i;
  BB           bb[15:0] (.T(~sd_wren), .I(i), .O(o), .B(sd_data));
  always @(posedge clk_in) i <= din;
//  OFS1P3BX dbo_FF[15:0] (.SCLK(sd_clk), .SP(1'b1),    .Q(i),  .D(din), .PD(1'b0));
  IFS1P3BX dbi_FF[15:0] (.SCLK(sd_clk), .SP(sd_rden), .Q(dout), .D(o), .PD(1'b0));
`endif
  
  // ---------------------------------------------------------------------
  // ------------------------ cycle state machine ------------------------
  // ---------------------------------------------------------------------
  
  // The state machine runs at 125Mhz, asynchronous to the CPU.
  // It idles doing refreshes and switches to burst reads and writes
  // as requested.
  
  localparam STBY  =   0;   // start state, do refreshes
  localparam RD    =  50;   // start of read sequence
  localparam WR    = 200;   // start of write sequence

  reg [8:0] t = 0;

  wire mreq =  rd | wr;

  always @(posedge clk_in) begin
    sd_cmd <= CMD_NOP;  // default command
    
    // move to next state
    t <= t + 1;

    if(reset != 0) begin // reset operation
      case(reset)
      99: begin sd_ba   <= 2'b00; sd_dqm  <= 2'b00;
                get     <= 1'b0;  put     <= 1'b0;
                sd_rden <= 1'b0;  sd_wren <= 1'b0;
          end
      41: sd_addr[10] <= 1'b1; // PRECHARGE ALL
      40: sd_cmd  <= CMD_PRECHARGE;
      30: sd_cmd  <= CMD_AUTO_REFRESH;
      20: sd_cmd  <= CMD_AUTO_REFRESH;
      11: sd_addr <= MODE;
      10: sd_cmd  <= CMD_LOAD_MODE;
       1: t <= STBY;
      endcase
    end
    
    else begin // normal operation
      case(t)

      // Idle doing refreshes
      //
      STBY:   begin
                sd_addr <= addr[21: 9];
                sd_ba   <= addr[23:22];
                if (mreq) begin
                  sd_cmd <= CMD_ACTIVE;
                  t <= rd ? RD : WR;
                end
              end

      STBY+1:  sd_cmd <= CMD_AUTO_REFRESH;
      STBY+10: t <= STBY;
      
      // Read burst, set-up for tRCD = 2 clocks and CL = 3 clocks. Possibly the burst
      // terminate could come earlier. The 'get' signal accounts for the dbi_FF delay.
      //
      RD+2:   sd_addr <= { 4'b0010, addr[8:0] }; 
      RD+3:   sd_cmd  <= CMD_READ;
      RD+6:   begin sd_rden <= 1'b1; get <= 1'b1; end                   
      // ... 128 read cycles
      RD+134: begin sd_rden <= 1'b0; get <= 1'b0; end                                   
      RD+136: sd_cmd  <= CMD_BURST_TERMINATE;    
      RD+137: sd_cmd  <= CMD_PRECHARGE; /* ALL */
      RD+140: t       <= STBY+1;
      
      // Write burst, set up for tRCD = 2. Burst terminate has to be exact here, and
      // the 'put' signal accounts for a 3 clock pipeline to fetch data.
      //
      WR+2:   put     <= 1'b1;                   
      WR+3:   sd_addr <= { 4'b0010, addr[8:0] }; 
      WR+4:   sd_wren <= 1'b1;
      WR+5:   sd_cmd  <= CMD_WRITE;              
      // ... 128 write cycles
      WR+130: put     <= 1'b0;                   
      WR+133: sd_cmd  <= CMD_BURST_TERMINATE;    
      WR+134: sd_wren <= 1'b0;                   
      WR+135: sd_cmd  <= CMD_PRECHARGE; /* ALL */
      WR+140: t       <= STBY+1;

      endcase
    end
  end

endmodule
