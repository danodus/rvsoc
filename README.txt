This repo contains a port of the RVSoc system as developed at the
University of Tokyo, ported to the ULX3S board.

RVSoc is a RV32imac based system-on-a-chip (SoC) that can run on a
modest FPGA. It is less than 4,000 lines of plain Verilog and is
Linux capable, implementing a standard Riscv RV32 MMU). It takes
about 14,000 LUTs and fits on the smallest ULX3S board.

There are other Linux capable implementations that are up to 10
times faster, but are also 10x larger.

The straightforward simplicity of RVSoc makes it an ideal base for
experimentation and for educational assignements.

A short Youtube clip demonstrates the system:
https://youtu.be/Kt_iXVAjXcQ

The discussion forum for this port is currently the ULX3S
Discord channel:
https://discord.com/channels/690209441953480758/1081150712479764530


To try Linux with RVSoc, the following steps are needed:
- Place linux_disk_img.bin on the SD card, starting at sector 0
  This will place a 16MB Buildroot file system on the SD card
- Concatenate the loader header and the linux kernel file:
  cat fujprog_loader_hdr.bin linux_mem_img.bin >knl.bin
- Use the fujprog loader program to load the bit-file and to
  upload the Linux (+ Berkely Boot Loader) image to ram:
  fujprog -t -e knl.bin -x 1000000 sys_85F.bit
- The upload will take about 2 minutes. After the upload, the
  monitor prompt will appear.
- At the monitor prompt, type 'x' and enter. The Linux boot will
  now start.
- At the login prompt type 'root', no password required.

Enjoy!


