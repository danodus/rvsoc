/*
 * Handle interrupts
 */

typedef unsigned int uint;

void putc(char c);
void puts(char *s);
void putw(int w);

uint r_mcause(void);
uint r_mie(void);
void w_mie(uint x);
uint r_mstatus(void);
void w_mstatus(uint x);
void w_mtvec(uint x);

void cons_intr(void);
void disk_intr(void);
void intrvec(void);

#define MSTATUS_MIE (1L << 3)
#define MIE_MEIE (1L << 11) 
#define MIE_MTIE (1L << 7) 
#define MIE_MSIE (1L << 3) 
#define PLIC_CLAIM ((uint*)(0x50200004))

void intr_init(void)
{
  w_mie(r_mie() | MIE_MEIE);
  w_mtvec((uint)&intrvec);
  w_mstatus(r_mstatus() | MSTATUS_MIE);
}

void intr(void)
{
  int scause = r_mcause();
  int icause = scause & 0xf;
  int irq;
  
  if(scause < 0) {
	switch(icause) {
	
	// 11 = M mode external interrupt
	case 11: {
		irq = *PLIC_CLAIM;
		     if(irq==1) cons_intr();
		else if(irq==2) disk_intr();
		else goto error;
		*PLIC_CLAIM = irq;
		return;
	}

	error: default:
		puts("unkown interrupt "); putw(irq); putc('\n');
	}
  }
  else {
	puts("exception "); putw(scause); putc('\n');
  }
}

