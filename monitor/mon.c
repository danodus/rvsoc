
#include "dtb.inc"

void cons_init(void);
void cons_rele(void);
void cons_write(char* s);
void putc(char c);
unsigned int getline(char* buf, unsigned int sz);

void disk_init(void);
void disk_rele(void);
unsigned int dsk_rw(unsigned int sec, unsigned int addr, int iswr);

void intr_init(void);

int param[3];

void puts(char *s)
{
	while(*s) putc(*s++);
}

void putw(int w)
{
	char *digit = "0123456789abcdef";
	char str[9];
	int i;
	
	str[8] = 0;
	for(i=7;i>=0;i--) {
		str[i] = digit[(w & 0xf)];
		w >>= 4;
	}
	puts(str);
}

void cmd_h(char *cmd)
{
	puts("h                - print help\n");
	puts("d addr [len]     - dump memory at addr, len bytes\n");
	puts("p addr val [len] - put val at addr, for len bytes\n");
	puts("r sec addr [cnt] - read sd sector to addr, for cnt sectors\n");
	puts("w sec addr [cnt] - write addr to sd sector, for cnt sectors\n");
	puts("m src dst len    - move src to dst, len bytes\n");
	puts("x [addr]         - jump to addr, default 0x80000000\n");
	puts("t                - do mem test\n");
}

void cmd_p(char *cmd, int prm)
{
	int i, *p;

	p = (int*)param[0];
	if(prm < 3) param[2] = 1;
	for(i=0; i<param[2]; i+=4) {
		*p++ = param[1];
	}
}

void cmd_d(char *cmd, int prm)
{
	int i, end;

	if(prm < 2) param[1] = 1;
	end = param[0] + param[1];
	for(i=param[0]; i<end; i+=4) {
		putw(i);
		putc(' ');
		putw(*((int*)i) );
		putc('\n');
	}
}

void cmd_m(int prm)
{
	int *src = (int*)(param[0]);
	int *dst = (int*)(param[1]);
	int len  = param[2] >> 2, *p;

	if(prm < 3 || len == 0) return;
	p = src+len;
	if(src < dst) {
		dst += len;
		while(p > src) *--dst = *--p;
	}
	else {
		while(src < p) *dst++ = *src++;
	}
}

void cmd_r(int prm, int iswr)
{
	int i;

	if(prm < 3) param[2] = 1;
	if(prm < 2) return;
	for(i=0; i<param[2]; i++) {
		dsk_rw(param[0], param[1], iswr);
		param[0] += 1;
		param[1] += 512;
	}
}

extern void do_goto(int addr);

void cmd_x(int prm)
{
	int i, *p;

	if(prm < 1) param[0] = 0x80000000;
	disk_rele();
	cons_rele();
	do_goto(param[0]);
}

void cmd_c(int prm)
{
	unsigned int i, c, end;
	int crc;

	end = param[0] + param[1];

	crc = 0;
	for(i=param[0];i<end;i++) {
		c = *((unsigned char*)i);
		crc = (crc<<1) + c + (crc < 0 ? 1 : 0);
	}
	puts("CRC = "); putw(crc); putc('\n');
}

void cmd_t(int prm)
{
	int i, j, end;

	if(prm < 3) param[2] = 3;
	if(prm < 2) {
		param[0] = 0x8000;
		param[1] = 0x8000;
	}
	puts("Mem test:\n");
	end = param[0] + param[1];

	if(param[2] & 1)
		for(i=param[0];i<end;i+=4) {
			*((int*)i) = i;
		}
	if(param[2] & 2)
		for(i=param[0];i<end;i+=4) {
			j = *((int*)i);
			if(i!=j) {
				putw(i);
				putc(' ');
				putw(*((int*)i) );
				putc('\n');
			}
		}
	puts("End run\n");
}

void skip(char *buf, int *ppos)
{
	char c, pos = *ppos;

	while(1) {
		c = buf[pos++];
		if((c>='0' && c<='9') || (c>='a' && c<='z')
		    || (c>='A' && c<='Z') || c==0)
			break;
	}
	*ppos = --pos;
}

int hexdgt(int c)
{
	if(c>='0' && c<='9') return c-'0';
	if(c>='a' && c<='f') return c-'a'+10;
	if(c>='A' && c<='F') return c-'A'+10;
	return -1;
}

int gethex(char *buf, int *ppos)
{
	int c, hex = 0, pos = *ppos;

	while(1) {
		c = hexdgt(buf[pos++]);
		if(c < 0) {
			*ppos = --pos;
			return hex;
		}
		hex = (hex << 4) + c;
	}
}

int getparam(char *buf, int *ppos)
{
	int i, pos = *ppos;
	
	param[0] = param[1] = param[2] = 0;
	for(i=0; i<3; i++) {
		skip(buf, &pos);
		if(hexdgt(buf[pos]) < 0) {
			*ppos = pos;
			return i;
		}
		param[i] = gethex(buf, &pos);
	}
	return 3;
}

#define PLIC(x) *(int*)(0x50000000 +(x))

void plic_init(void)
{
	PLIC(     0x4) = 1;   // int 1 has prio 1
	PLIC(     0x8) = 2;   // int 2 has prio 1
	PLIC(  0x2000) = 6;   // enable int 1 & 2 for ctx 0
	PLIC(0x200000) = 0;   // ctx 0 threshold is 0
}

void main()
{
	int prm, pos;
	char buf[80];

	plic_init();
	intr_init();
	cons_init();
	//cons_write("string via console write\n");
	disk_init();
	puts("\n\nRVSoc Mini Monitor (ULX3S) - use 'h' command for help\n\n");
	while(1) {
		puts("> ");
		getline(buf, 80);
		pos = 1;
		prm = getparam(buf, &pos);
		switch(buf[0]) {
		case 'h': cmd_h(buf);      break;
		case 't': cmd_t(prm);      break;
		case 'p': cmd_p(buf, prm); break;
		case 'd': cmd_d(buf, prm); break;
		case 'm': cmd_m(prm);      break;
		case 'r': cmd_r(prm, 0);   break;
		case 'w': cmd_r(prm, 1);   break;
		case 'c': cmd_c(prm);      break;
		case 'x': cmd_x(prm);      break;
		}
	}
}
