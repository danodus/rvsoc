/*
 * RVSoC mini monitor -- disk interface
 *
 * This source code is public domain
 *
 */
 
// virtio defs and register for the console output stream:
// - keyboard is channel 0
// - use three buffers (next power of two is 4)
//
#define NUM 4
#include "virtio.h"

#define R(r) ((volatile uint32 *)(0x41000000 + (r)))

static struct virtq_desc  buf[3];  
static struct virtq_avail drv; 
static struct virtq_used  dev;

// init console keyboard virtio queue
//
void disk_init(void)
{
	// Four channels available, but just use channel 0
	*R(VIRTIO_MMIO_QUEUE_SEL)       = 0;
	*R(VIRTIO_MMIO_QUEUE_DESC_LOW)  = (uint32) &buf;
	*R(VIRTIO_MMIO_DRIVER_DESC_LOW) = (uint32) &drv;
	*R(VIRTIO_MMIO_DEVICE_DESC_LOW) = (uint32) &dev;
	*R(VIRTIO_MMIO_QUEUE_NUM)       = NUM;

	// all our requests start with descriptor 0
	drv.ring[0] = 0;
	drv.ring[1] = 0;
	drv.ring[2] = 0;
	drv.ring[3] = 0;
	
	*R(VIRTIO_MMIO_QUEUE_READY) = 0x1;
}

void disk_rele()
{
	*R(VIRTIO_MMIO_QUEUE_READY) = 0x0;
}

unsigned int dsk_rw(unsigned int sec, unsigned int addr, int iswr)
{
	struct virtio_blk_req	dskreq;
	unsigned int		idx_seen;
	unsigned char		status;

	dskreq.type     = iswr ? VIRTIO_BLK_T_OUT : VIRTIO_BLK_T_IN;
	dskreq.reserved = 0;
	dskreq.sector   = sec;
	
	buf[0].addr  = (uint32) &dskreq;
	buf[0].len   = sizeof(dskreq);
	buf[0].flags = VRING_DESC_F_NEXT;
	buf[0].next  = 1;

	buf[1].addr  = (uint32) addr;
	buf[1].len   = 512;
	buf[1].flags = VRING_DESC_F_NEXT | (iswr ? VRING_DESC_F_WRITE : 0);
	buf[1].next  = 2;
	
	buf[2].addr  = (uint32) &status;
	buf[2].len   = 1;
	buf[2].flags = VRING_DESC_F_WRITE;
	buf[2].next  = 0;
	
	idx_seen = dev.idx;
	drv.idx++;
	*R(VIRTIO_MMIO_QUEUE_NOTIFY) = 0; // kick channel 0 into action
	
	while(1) {
		if(dev.idx!=idx_seen) {
			return status; // 0x00 for success
		}
	}
}

void disk_intr(void)
{
	//putc('D');
	*R(VIRTIO_MMIO_INTERRUPT_ACK) = 2;
}
