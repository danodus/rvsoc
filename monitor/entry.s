TEXT entry(SB),0,$-8
	MOV	$16384,R2
	MOV	$setSB(SB),R3

	MOV	$edata(SB),R8	// clear bss area
	MOV	$end(SB),R5
	MOV	R0,0(R8)
	ADD	$4,R8,R8
	BLT	R5,R8,-2(PC)

	JAL	R1,main(SB)
	JMP	0(PC)
	END


TEXT do_goto+0(SB),0,$-8	// jump to location R8
	MOV	$0x81000000,R11 // for BBL/Linux
	MOVW	$0,CSR(0x0304)  // switch off interrupts
	JALR	R0,0(R8)
	RET			// never happens

TEXT r_mstatus+0(SB),0,$0
	MOVW	CSR(0x0300),R8
	RET

TEXT w_mstatus+0(SB),0,$0
	MOVW	R8,CSR(0x0300)
	RET

TEXT r_mcause+0(SB),0,$0
        MOVW    CSR(0x0342),R8
        RET

TEXT r_mie+0(SB),0,$0
	MOVW	CSR(0x0304),R8
	RET

TEXT w_mie+0(SB),0,$0
	MOVW	R8,CSR(0x0304)
	RET

TEXT w_mtvec+0(SB),0,$0
        MOVW    R8,CSR(0x0305)
        RET

#define MRET            WORD $0x30200073

TEXT intrvec(SB),0,$-8
        // make room to save registers.
        SUB     $64,R2,R2

        MOV R1,    0(R2)
        MOV R2,    4(R2)
        MOV R3,    8(R2)
        MOV R4,   12(R2)
        MOV R5,   16(R2)
        MOV R6,   20(R2)
        MOV R7,   24(R2)
        MOV R8,   28(R2)
        MOV R9,   32(R2)
        MOV R10,  36(R2)
        MOV R11,  40(R2)
        MOV R12,  44(R2)
        MOV R13,  48(R2)
        MOV R14,  52(R2)
        MOV R15,  56(R2)

        JAL     ,intr(SB)

        MOV   0(R2), R1
        MOV   4(R2), R2
        MOV   8(R2), R3
        MOV  12(R2), R4
        MOV  16(R2), R5
        MOV  20(R2), R6
        MOV  24(R2), R7
        MOV  28(R2), R8
        MOV  32(R2), R9
        MOV  36(R2), R10
        MOV  40(R2), R11
        MOV  44(R2), R12
        MOV  48(R2), R13
        MOV  52(R2), R14
        MOV  56(R2), R15

        ADD     $64,R2,R2
	MRET
